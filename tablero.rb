class ColumnaLlenaException < Exception
end

class Tablero
    def initialize
        @fichas = Array.new(7) { Array.new(6, 0) }
    end

    def add_ficha(columna, jugador)
        ind_columna = columna - 1

        fila = @fichas[ind_columna].rindex(0)
        throw ColumnaLlenaException if fila == nil

        @fichas[ind_columna][fila] = jugador
    end

    def mostrar(uno = "O", dos = "X", vacio = "_")
        tokens = [vacio, uno, dos]

        superior = (Array.new(7) { |i| i + 1 }).join(" ")
        filas_mostrar =
            (
                Array.new(6) do |i|
                    (@fichas.map { |columna| tokens[columna[i]] }).join(" ")
                end
            )

        return [superior, filas_mostrar].flatten().join("\n")
    end

    def ganador
        casos =
            @fichas.map.with_index do |columna, ind_columna|
                columna.map.with_index do |fila, ind_fila|
                    puede_norte = ind_fila >= 3
                    puede_sur = ind_fila <= columna.length - 4
                    puede_este = ind_columna <= @fichas.length - 4
                    puede_oeste = ind_columna >= 3

                    lineas = []

                    if puede_sur
                        linea = Array.new(4) { |i| @fichas[ind_columna][ind_fila + i] }
                        lineas = [*lineas, linea]
                    end

                    if puede_norte
                        linea = Array.new(4) { |i| @fichas[ind_columna][ind_fila - i] }
                        lineas = [*lineas, linea]
                    end

                    if puede_este
                        linea = Array.new(4) { |i| @fichas[ind_columna + i][ind_fila] }
                        lineas = [*lineas, linea]
                    end

                    if puede_oeste
                        linea = Array.new(4) { |i| @fichas[ind_columna - i][ind_fila] }
                        lineas = [*lineas, linea]
                    end

                    if puede_sur && puede_este
                        linea = Array.new(4) { |i| @fichas[ind_columna + i][ind_fila + i] }
                        lineas = [*lineas, linea]
                    end

                    if puede_norte && puede_oeste
                        linea = Array.new(4) { |i| @fichas[ind_columna - i][ind_fila - i] }
                        lineas = [*lineas, linea]
                    end

                    if puede_sur && puede_oeste
                        linea = Array.new(4) { |i| @fichas[ind_columna - i][ind_fila + i] }
                        lineas = [*lineas, linea]
                    end

                    if puede_norte && puede_este
                        linea = Array.new(4) { |i| @fichas[ind_columna + i][ind_fila - i] }
                        lineas = [*lineas, linea]
                    end

                    lineas.any? { |linea| linea.all?(fila) } && fila != 0 ? fila : nil
                end
            end

        resultado = (casos.flatten().filter { |x| x != nil }).first

        if resultado == nil &&
                  @fichas.all? { |columna| columna.all? { |fila| fila != 0 } }
            return 0
        else
            return resultado
        end
    end
end
