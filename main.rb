require_relative "tablero"

#Write your code here
def main
    jugadores =
        Array.new(2) do |i|
            puts "Nombre de jugador #{i + 1}:"
            (gets).strip
        end

    iniciales = jugadores.map { |nombre| nombre[0].capitalize }
    mismas_iniciales = iniciales.all?(iniciales.first)

    tablero = Tablero.new
    turno = 0

    while tablero.ganador == nil
        begin
            puts(
                if mismas_iniciales
                    tablero.mostrar
                else
                    tablero.mostrar(iniciales[0], iniciales[1])
                end
            )
            puts "Turno de #{jugadores[turno]}. Elige columna:"

            columna_elegida = (gets).to_i
            if columna_elegida < 1
                # No queremos que 0 signifique 7 para el programa
                throw StandardError
            end
            tablero.add_ficha columna_elegida, (turno + 1)

            turno = (turno + 1) % 2
        rescue StandardError
            puts "Ups, eso no ha funcionado. Prueba otra vez."
        end
    end

    puts(
        if mismas_iniciales
            tablero.mostrar
        else
            tablero.mostrar(iniciales[0], iniciales[1])
        end
    )
    resultado =
        (
            if tablero.ganador == 0
                "Empate"
            else
                "El ganador es #{jugadores[tablero.ganador - 1]}"
            end
        )

    puts resultado
end

main
